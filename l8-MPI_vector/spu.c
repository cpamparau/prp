#include <stdio.h>
#include <stdio.h>
#include <spu_mfcio.h>
#define SIZE 1024
#define TAG 1
#define NUMBER_OF_NUMBERS 11

unsigned int buff[SIZE] __attribute__ ((aligned(128)));

int main(unsigned long long spe_id, unsigned long long argp, unsigned long long envp)
{
        unsigned int i;
        mfc_get(buff, argp, sizeof(buff), TAG, 0, 0);
        mfc_write_tag_mask(1<<TAG);
        mfc_read_tag_status_all();
        int suma = 0, primit;
        for(i = 0; i < NUMBER_OF_NUMBERS; i++){
                suma+=buff[i];
        }
        while(! spu_stat_out_mbox());
        printf("Trimit suma %d la PPU\n",suma);
        spu_write_out_mbox(suma);
        return 0;
}
