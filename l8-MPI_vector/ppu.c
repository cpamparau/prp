#include <stdio.h>
#include <stdlib.h>
#include <libspe2.h>
#include <pthread.h>
#include <mpi.h>
#include <sys/time.h>
#define MAX_SPU_THREADS 6
#define NP 6
#define N 1024

extern spe_program_handle_t spu;
int 	  buffer[N];
void *ppu_pthread_function(void *arg) {
    spe_context_ptr_t ctx;
    unsigned int entry = SPE_DEFAULT_ENTRY;

    ctx = *((spe_context_ptr_t *)arg);
    if (spe_context_run(ctx, &entry, 0, buffer, NULL, NULL) < 0) {
	perror ("Failed running context");
	exit (1);
    }
    pthread_exit(NULL);
}
int main(int argc, char **argv) {
   int    rank, data1;
   int    nprocs, ppu_sum=0, mbox_data;
   char   host[100];
   int i, spu_threads;
   unsigned int  master_sum = 0, master_data;
    spe_context_ptr_t ctxs[MAX_SPU_THREADS];
    pthread_t threads[MAX_SPU_THREADS];

    MPI_Init (&argc, &argv);
    MPI_Comm_size (MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    gethostname(host, 100);
	
    if (rank == 0)
    {
        for(i = 0; i < N; i++)buffer[i] = i;
        for(i = 1; i < NP ; i++) MPI_Send(&buffer, N, MPI_INTEGER, i, 0, MPI_COMM_WORLD);
         //receive data from slave and compute final result
        for(i = 1; i< MAX_SPU_THREADS; i=i+2)
        {
            MPI_Recv(&master_data, 1, MPI_INTEGER, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            printf("\t[MASTER] Am primit de la PPU %d data %d\n", i, master_data);
            master_sum = master_sum + master_data;
        }
        printf("[MASTER]Rezultat final %d\n",master_sum );
    }else{
	MPI_Recv(&buffer, N, MPI_INTEGER, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        if(rank%2 == 0)
        {
        //    MPI_Send(&master_sum, 1, MPI_INTEGER, 0, 0, MPI_COMM_WORLD);
        }
        if(rank%2 != 0){    	
	    //Determine the number of SPE threads to create.
	    spu_threads = spe_cpu_info_get(SPE_COUNT_USABLE_SPES, -1);
	    if (spu_threads > MAX_SPU_THREADS) spu_threads = MAX_SPU_THREADS;

	    //Create several SPE-threads to execute 'spu'.
	    for(i=0; i<spu_threads; i++) {
		// Create context
		if ((ctxs[i] = spe_context_create (0, NULL)) == NULL) {
		    perror ("Failed creating context");
		    exit (1);
		}
		// Load program into context
		if (spe_program_load (ctxs[i], &spu)) {
		    perror ("Failed loading program");
		    exit (1);
		}
		// Create thread for each SPE context
		if (pthread_create (&threads[i], NULL, &ppu_pthread_function, &ctxs[i])) {
		    perror ("Failed creating thread");
		    exit (1);
		}
	    }
            //wait receive data from SPU and compute his parent job
            for(i=0 ; i<spu_threads; i++)
            {
                while(!spe_out_mbox_status(ctxs[i]));
                spe_out_mbox_read(ctxs[i], &mbox_data, 1);
                printf("[PPU] am primit %d \n", mbox_data);
                //add data from SPU
                ppu_sum = ppu_sum + mbox_data;
            }
            MPI_Send(&ppu_sum, 1, MPI_INTEGER, 0, 0, MPI_COMM_WORLD);
            
             // Wait for SPU-thread to complete execution.
	    for (i=0; i<spu_threads; i++) {
		if (pthread_join (threads[i], NULL)) {
		    perror("Failed pthread_join");
		    exit (1);
		}
		// Destroy context
		if (spe_context_destroy (ctxs[i]) != 0) {
		    perror("Failed destroying context");
		    exit (1);
		}
	    }
      } // else rank%2!=0
     } // else rank == 0    	
    MPI_Finalize();
   return 0;
}
